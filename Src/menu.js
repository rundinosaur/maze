var menu = cc.LayerColor.extend({

    _won:false,
    _pointsEarned:0,
   player: null,
    ctor:function() {
        this._super();
        cc.associateWithNative( this, cc.LayerColor );
    },

    onEnter:function () {

        this._super();
      
       
        var director = cc.Director.getInstance();
        var winSize = director.getWinSize();
           var background = cc.Sprite.create(s_background1);
        background.setPosition(winSize.width/2 , -550);
        this.addChild(background,0);
        var centerPos = cc.p( winSize.width/2, winSize.height/2 );
        var selectPlay = cc.Sprite.create(s_play_s);
        var noSelectPlay = cc.Sprite.create(s_play_n);
        var menuItemPlay= cc.MenuItemSprite.create(
            cc.Sprite.create(s_menuPlay_n), 
             cc.Sprite.create(s_menuPlay_n),
            
            this.playAgain, this);


      
        var dare = cc.MenuItemSprite.create(
             cc.Sprite.create(s_dare_n),
            cc.Sprite.create(s_dare_s), 
            this.playAgain, this);

         dare.setPosition(0,-75);

        var scores = cc.MenuItemSprite.create(
             cc.Sprite.create(s_scoreButton),
            cc.Sprite.create(s_scoreButton), 
            this.playAgain, this);

         scores.setPosition(0,-150);
       // var menu = cc.Menu.create(menuItemPlay); 
       var tryAgainButton = cc.MenuItemFont.create("Play Again",this.playAgain,this);
            
       //cc.MenuItemFont.create("Play Again",this.playAgain,this);
       
       //tryAgainButton.setColor(cc.c3b(0, 155, 0));
       var menu = cc.Menu.create(tryAgainButton, menuItemPlay, dare,scores); 
       menu.setPosition(winSize.width/2,(winSize.height/2));
       this.addChild(menu, 45);

     
         player = cc.Sprite.create(s_player0);
        player.setPosition(winSize.width / 2, 125);
        player.setScale(.5);
        this.addChild(player, 45);

    },

    playAgain:function(){

            console.log("Do something");
		
             var scene = MainLayer.scene();
            cc.Director.getInstance().replaceScene(scene);//This needs to create a new scene not create a new instance. 
    },
    update:function(dt){

    	
    },

    dare:function(){
        console.log("InitiateDare");

        window.location.href='http://twitter.com/share?url=http://carpooldigital.com&text=I just scored ' +this._pointsEarned+ ' points in MAZE !&count=horiztonal&via=spoofapps&related=solitarydesigns';
    
    },
    score:function(){
         var scene = scores.scene();
            cc.Director.getInstance().replaceScene(scene);//This needs to create a new scene not create a new instance. 

    }
});

menu.create = function (won , score) {
    var sg = new menu();
    sg._won = won;
    sg._pointsEarned = score; 
    if (sg && sg.init(cc.c4b(200, 255, 100, 230))) {
        return sg;
    }
    return null;
};

menu.scene = function (won, score) {
    var scene = cc.Scene.create();
    var layer = menu.create(won , score);
    scene.addChild(layer);
    return scene;
};