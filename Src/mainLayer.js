var audioEngine = cc.AudioEngine.getInstance();
var MainLayer = cc.LayerColor.extend({

    _blocks:[],
    _points:[],
    _bees:[],
    player:null,
    gotPoint:false,
    _blocksDestroyed:0,
    _pointsEarned:0,
    _speed:1, 
    _scheduleBlocks:1, 
    _size:1,
    _blockTypes:[1,2,3],
    _pathDirection:[0,64,-64],
    _pathOneXLocation: 0, 
    _pathTwoXLocation: 0, 
    _shouldSpawn:false, 
    _powerUps:{
        ICanFly: 10,
        ICanBrakeWall: 0
    }, 
    highScore: 0, 
    _spawnPowerUp:[1,2,3,4],
    _oldTime: 0,
	_scaleTime: 0,
    background:null, 
    background2:null,
    countDown: 3 ,
    countDownAfter:10,
    countDownDied:10,
    countDownSprite:null, 
    countDownLabel:null,
    lifeLabel: 0,
    flyingAction:null,
    spriteSheet:null,
    indexNumber:0,
    doCountDown : true,
    doEndCountDown: 0, 
    backgroundCount:0,
    showEndCountDown: 0,
    countDownLabel2:null,
    startSecondCountDown :false,
    pinLocation: true,
    ctor:function() {

        // Rest of file...
        this._super();

        cc.associateWithNative( this, cc.LayerColor );




        //Find a way to store all of those block types into an array. 
    },

    onEnter:function () {
        this._super();
        this.scrollBackground(); 
         this.showCountDown();
            var background3 = cc.Sprite.create(s_background1);
        background3.setPosition(winSize.width/2 , -550);
        this.addChild(background3,0);
        if( 'touches' in sys.capabilities ) {
            this.setTouchEnabled(true);
        }
        if( 'mouse' in sys.capabilities ) {
            this.setMouseEnabled(true);
        }

        this.highScore = store.get("highScore");
        player = cc.Sprite.create(s_player0);        
        player.setPosition(winSize.width / 2, 125);
        player.setScale(.5);
        this.addChild(player, 30);
        
        cc.SpriteFrameCache.getInstance().addSpriteFrames(s_playPlist);
        //var playerTexture = cc.textureCache.addImage(s_player);
        var playerImages  = cc.SpriteBatchNode.create(s_player);
         this.addChild(playerImages);
         var animFrames = [];
        
        for (var i = 0; i < 2; i++) {
        var str = "player" + i + ".png";
        var frame = cc.SpriteFrameCache.getInstance().getSpriteFrame(str);
        animFrames.push(frame);
        }

        var animation = cc.Animation.create(animFrames, .1);
        var flyingAction = cc.RepeatForever.create(cc.Animate.create(animation));

         

        player.runAction(flyingAction); 




        
        
        var touchPad = cc.Sprite.create(s_touchPad);
        touchPad.setPosition(winSize.width / 2, 40)

        this.addChild(touchPad, 45); 

   
   


 
        var hud = cc.Sprite.create(s_hud);
        hud.setPosition(winSize.width/2 , 561);
        this.addChild(hud,27);

        tcircle = cc.Sprite.create(s_circle);
        tcircle.setPosition(winSize.width/2 , 40);

        this.addChild(tcircle, 46);
      
        this.schedule(this.backgroundLogic, 18.5); 
        
        //this.schedule(this.addPoints,1);
        this.scheduleUpdate();
        var title = cc.LabelTTF.create("Fly Lady Bug", "Helvetica", 20);
        title.setColor(cc.c3(0,0,0));
        title.setPosition(cc.p(winSize.width/2,555));
        this.addChild(title,28);
        score = cc.LabelTTF.create("0", "Helvetica", 20);
        score.setColor(cc.c3(0,0,0));//black color
        score.setPosition(cc.p(15, 555));
        this.addChild(score, 28);
        lifeLabel = cc.LabelTTF.create("0", "Helvetica", 20);
        lifeLabel.setColor(cc.c3(0,0,0));
        lifeLabel.setPosition(cc.p(winSize.width - 25,555));
        this.addChild(lifeLabel, 28);
        var lifeSprite = cc.Sprite.create(s_player0);
        lifeSprite.setPosition(winSize.width - 10 , 557);
        lifeSprite.setScale(.3);
        this.addChild(lifeSprite,28);

     


        audioEngine.playMusic(s_bgMusic, true);
    },


    addblock:function() {
        var randomBlock = (parseInt((Math.random() * 6)));
        var pathDir = parseInt((Math.random() * 3)); 
        var pathDir2 = parseInt((Math.random()*3));
       // var block0 = cc.Sprite.create(this._tileOneType[1]);
        var block1 = cc.Sprite.create(s_tile7);//We can do array[randomize Index] to randomize tile 
     
        var pointCount = cc.Sprite.create(s_pointCount);
        
       

        var minX = block1.getContentSize().width / 2;
        var maxX = winSize.width - block1.getContentSize().width / 2;
        var rangeX = -125;
        var actualX = (Math.random() * rangeX) + minX; // 1
       
   
     
        block1.setPosition(actualX +97, winSize.height + 50);//Was +97
        this.addChild(block1, 4); // 2
   

 
        if(randomBlock == 1){
                console.log("1"); 
                var block0 = cc.Sprite.create(s_leftTile);
                var block1 = cc.Sprite.create(s_middleTile);
                var block2 = cc.Sprite.create(s_rightTile);
                var block3 = cc.Sprite.create(s_oneTile);

            block0.setPosition( 36, winSize.height+50);//Was + 33
            this.addChild(block0, 7); // 2
            block1.setPosition(97, winSize.height+50 );//Was +97
            this.addChild(block1, 4); // 2
            block2.setPosition(160, winSize.height+50);//Was +225
            this.addChild(block2, 5); // 2
            block3.setPosition( 289, winSize.height+50);//Was + 289
            this.addChild(block3, 6);

            var block0Move = cc.MoveTo.create(4, cc.p( 36, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 97, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 160, -50));
            var block3Move = cc.MoveTo.create(4, cc.p( 289, -50));
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();

               
            }, this);


            block0.runAction(cc.Sequence.create(block0Move, actionMoveDone));
            block1.runAction(cc.Sequence.create(block1Move, actionMoveDone));
            block2.runAction(cc.Sequence.create(block2Move, actionMoveDone));
            block3.runAction(cc.Sequence.create(block3Move,actionMoveDone));

            block0.setTag(1);
            block1.setTag(1);
            block2.setTag(1);
            block3.setTag(1);



            this._blocks.push(block0);
            this._blocks.push(block1);
            this._blocks.push(block2);
            this._blocks.push(block3);
        }else if(randomBlock == 2){
            console.log("2");
            var block0 = cc.Sprite.create(s_oneTile);
            var block1 = cc.Sprite.create(s_oneTile);
            var block2 = cc.Sprite.create(s_oneTile);

            block0.setPosition( 33, winSize.height+50);//Was + 33
            this.addChild(block0, 7); // 2
            block1.setPosition(163, winSize.height+50);//Was +97
            this.addChild(block1, 4); // 2
            block2.setPosition(289, winSize.height+50);//Was +225
            this.addChild(block2, 5); // 2

            var block0Move = cc.MoveTo.create(4, cc.p( 33, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 163, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 289, -50));
            
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();
            }, this);


            block0.runAction(cc.Sequence.create(block0Move, actionMoveDone));
            block1.runAction(cc.Sequence.create(block1Move, actionMoveDone));
            block2.runAction(cc.Sequence.create(block2Move, actionMoveDone));
            

            var block0Move = cc.MoveTo.create(4, cc.p( 33, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 97, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 163, -50));

            block0.setTag(1);
            block1.setTag(1);
            block2.setTag(1);
            this._blocks.push(block0);
            this._blocks.push(block1);
            this._blocks.push(block2);

        }else if(randomBlock == 3){
            console.log("3");
                var block0 = cc.Sprite.create(s_leftTile);
                var block1 = cc.Sprite.create(s_middleTile);
                var block2 = cc.Sprite.create(s_middleTile);
                var block3 = cc.Sprite.create(s_rightTile);

            block0.setPosition( 105, winSize.height+50);//Was + 33
            this.addChild(block0, 7); // 2
            block1.setPosition(161, winSize.height+50 );//Was +97
            this.addChild(block1, 4); // 2
            block2.setPosition(225, winSize.height+50);//Was +225
            this.addChild(block2, 5); // 2
            block3.setPosition( 289, winSize.height+50);//Was + 289
            this.addChild(block3, 6);

            var block0Move = cc.MoveTo.create(4, cc.p( 105, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 161, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 225, -50));
            var block3Move = cc.MoveTo.create(4, cc.p( 289, -50));
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();

               
            }, this);


            block0.runAction(cc.Sequence.create(block0Move, actionMoveDone));
            block1.runAction(cc.Sequence.create(block1Move, actionMoveDone));
            block2.runAction(cc.Sequence.create(block2Move, actionMoveDone));
            block3.runAction(cc.Sequence.create(block3Move,actionMoveDone));

            block0.setTag(1);
            block1.setTag(1);
            block2.setTag(1);
            block3.setTag(1);



            this._blocks.push(block0);
            this._blocks.push(block1);
            this._blocks.push(block2);
            this._blocks.push(block3);
        }else if(randomBlock == 4){
            console.log("4")
                var block0 = cc.Sprite.create(s_leftTile);
                var block1 = cc.Sprite.create(s_middleTile);
                var block2 = cc.Sprite.create(s_middleTile);
                var block3 = cc.Sprite.create(s_rightTile);

            block0.setPosition( 37, winSize.height+50);//Was + 33
            this.addChild(block0, 7); // 2
            block1.setPosition(100, winSize.height+50 );//Was +97
            this.addChild(block1, 4); // 2
            block2.setPosition(163, winSize.height+50);//Was +225
            this.addChild(block2, 5); // 2
            block3.setPosition( 225, winSize.height+50);//Was + 289
            this.addChild(block3, 6);

            var block0Move = cc.MoveTo.create(4, cc.p( 37, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 100, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 163, -50));
            var block3Move = cc.MoveTo.create(4, cc.p( 225, -50));
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();

               
            }, this);


            block0.runAction(cc.Sequence.create(block0Move, actionMoveDone));
            block1.runAction(cc.Sequence.create(block1Move, actionMoveDone));
            block2.runAction(cc.Sequence.create(block2Move, actionMoveDone));
            block3.runAction(cc.Sequence.create(block3Move,actionMoveDone));

            block0.setTag(1);
            block1.setTag(1);
            block2.setTag(1);
            block3.setTag(1);



            this._blocks.push(block0);
            this._blocks.push(block1);
            this._blocks.push(block2);
            this._blocks.push(block3);
        }else if(randomBlock == 5){
            console.log("5");
                var block0 = cc.Sprite.create(s_leftTile);
                var block1 = cc.Sprite.create(s_middleTile);
                var block2 = cc.Sprite.create(s_rightTile);
                

            block0.setPosition( 100, winSize.height+50);//Was + 33
            this.addChild(block0, 7); // 2
            block1.setPosition(163, winSize.height+50 );//Was +97
            this.addChild(block1, 4); // 2
            block2.setPosition(225, winSize.height+50);//Was +225
            this.addChild(block2, 5); // 2
            

            var block0Move = cc.MoveTo.create(4, cc.p( 100, -50));
            var block1Move = cc.MoveTo.create(4, cc.p( 163, -50));
            var block2Move = cc.MoveTo.create(4, cc.p( 225, -50));
           
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();

               
            }, this);


            block0.runAction(cc.Sequence.create(block0Move, actionMoveDone));
            block1.runAction(cc.Sequence.create(block1Move, actionMoveDone));
            block2.runAction(cc.Sequence.create(block2Move, actionMoveDone));
            

            block0.setTag(1);
            block1.setTag(1);
            block2.setTag(1);
          



            this._blocks.push(block0);
            this._blocks.push(block1);
            this._blocks.push(block2);
          
        }else if(randomBlock == 0 ){

            console.log("0");
             var fence0 = cc.Sprite.create(s_fence);
             var fence1 = cc.Sprite.create(s_fence);
     

            fence0.setPosition( 0, winSize.height+50);//Was + 33
            this.addChild(fence0, 7); // 2
            fence1.setPosition(winSize.width, winSize.height+50);//Was +97
            this.addChild(fence1, 4); // 2
          

            var fence0Move = cc.MoveTo.create(4, cc.p( 0, -50));
            var fence1Move = cc.MoveTo.create(4, cc.p( winSize.width, -50));
            
           
              var actionMoveDone = cc.CallFunc.create(function(node) { // 4
                cc.ArrayRemoveObject(this._blocks, node); // 5
                node.removeFromParent();
            }, this);


            fence0.runAction(cc.Sequence.create(fence0Move, actionMoveDone));
            fence1.runAction(cc.Sequence.create(fence1Move, actionMoveDone));
           
            

         
            fence0.setTag(1);
            fence1.setTag(1);
       
            this._blocks.push(fence0);
            this._blocks.push(fence1);
       

            //Small wood fence 1 on the side . 
        }
      
        pointCount.setPosition(actualX + 161 , winSize.height + 50);
        this.addChild(pointCount, 12);
        

         var actualDuration =4;


         var actionMove3 = cc.MoveTo.create(actualDuration, cc.p(actualX + 161, -50));//This controls the point block 
        


        this._pathOneXLocation = pathDir; 
        this._pathTwoXLocation = this._pathOneXLocation;


        var actionMoveDone2 = cc.CallFunc.create(function(node) { // 4
            cc.ArrayRemoveObject(this._blocks, node); // 5
            node.removeFromParent();
           
        }, this);

  
        pointCount.runAction(cc.Sequence.create(actionMove3,actionMoveDone2));
        


 
        pointCount.setTag(2);
       
        this._points.push(pointCount);
 



    },

    scrollBackground:function(){

        //Research how to scroll the background 
        background = cc.Sprite.create(s_background1);
        background.setPosition(winSize.width/2 , winSize.height+2200);
        this.addChild(background,2);

        var actionMove = cc.MoveTo.create(25.5, cc.p(winSize.width/2,-1400)); // This controls the powerup block
        var actionMoveDone = cc.CallFunc.create(function(node) { // 4
           // cc.ArrayRemoveObject(this._blocks, node); // 5
            node.removeFromParent();
        }, this);

        background.runAction(cc.Sequence.create(actionMove,actionMoveDone)); 
       
    },

    scrollBackground2:function(){

        var background2 = cc.Sprite.create(s_background1);
        background2.setPosition(winSize.width/2 , -550);
        this.addChild(background2,1);

        var actionMove = cc.MoveTo.create(8.9, cc.p(winSize.width/2,-2000)); // This controls the powerup block
        var actionMoveDone = cc.CallFunc.create(function(node) { // 4
           // cc.ArrayRemoveObject(this._blocks, node); // 5
          
          node.removeFromParent();
        }, this);



        background2.runAction(cc.Sequence.create(actionMove,actionMoveDone)); 


    },

    playerDied:function(){
        if(this.startSecondCountDown == true){

            if(this.countDownDied>=10){
                countDownLabel2.setString("10");
                this.countDownDied--;
            }else if(this.countDownDied>=9){
                countDownLabel2.setString("9");
                this.countDownDied--;
            }else if (this.countDownDied >=8){
                countDownLabel2.setString("8");
                this.countDownDied--;
            }else if(this.countDownDied >= 7){
                countDownLabel.setString("7");
                this.countDownDied--;
            }else if(this.countDownDied>=6){
                countDownLabel2.setString("6");
                this.countDownDied--;
            }else if(this.countDownDied>=5){
                countDownLabel2.setString("5");
                this.countDownDied--;
            }else if (this.countDownDied >=4){
                countDownLabel2.setString("4");
                this.countDownDied--;
            }else if(this.countDownDied >= 3){
                //Lable 3 
                console.log("Label:3");
                countDownLabel2.setString("3");
                this.countDownDied--;
            }else if(this.countDownDied >= 2){
                //lable 2
                console.log("Label:2");
                countDownLabel2.setString("2");
                this.countDownDied--;
            }else if(this.countDownDied >=1){
                //lable 1 
                console.log("Label:1");
                countDownLabel2.setString("1");
                 this.countDownDied--;
            }else{
                //remove node. 
                console.log("Remove Label");
                 this.countDownDied--;
                 console.log(this.countDownDied);
                 // countDownSprite.removeFromParent();
                 player.removeFromParent();
                  var scene = GameOver.scene(false, this._pointsEarned);
                     cc.Director.getInstance().replaceScene(scene);
                    
                     if(this._pointsEarned > this.highScore){
                           
                            store.set('highScore', this._pointsEarned);
                        }
            }

        }
    },

    addPoints:function(){

     
        this._pointsEarned++; 
        score.setString((this._pointsEarned).toString());
                
        
    },

    saveScore:function(pSender) {
        sys.localStorage.setItem("bool1",false);
        //String types only support, read use need to change
        sys.localStorage.setItem("num2","22");
        },

    spawnPowerUps:function(){
        if(shouldSpawn = true){
        var rangeX = -125;
        //  var minX = sizePowerUp.getContentSize().width / 2;
        // var actualX = (Math.random() * rangeX) + minX;


    

    var sizePowerUp = cc.Sprite.create(s_bee0);
        sizePowerUp.setPosition(-10, winSize.height +135);//Was 135
        this.addChild(sizePowerUp, 30);

         cc.SpriteFrameCache.getInstance().addSpriteFrames(s_beePlist);
         var beeImages  = cc.SpriteBatchNode.create(s_bigPowerUp);
        this.addChild(beeImages);
         var animFrames = [];


          for (var i = 0; i < 2; i++) {
            var str = "bee" + i + ".png";
            var frame = cc.SpriteFrameCache.getInstance().getSpriteFrame(str);
            animFrames.push(frame);
            }

        var animation = cc.Animation.create(animFrames, .1);
        var flyingAction = cc.RepeatForever.create(cc.Animate.create(animation));

         

        sizePowerUp.runAction(flyingAction); 


        var actionMove4 = cc.MoveTo.create(4, cc.p(winSize.width/2,35)); // This controls the powerup block
        var actionMoveDone3 = cc.CallFunc.create(function(node) { // 4
           // cc.ArrayRemoveObject(this._blocks, node); // 5
            node.removeFromParent();
           
        }, this);

        sizePowerUp.runAction(cc.Sequence.create(actionMove4,actionMoveDone3)); 
         sizePowerUp.setTag(3);
         this._bees.push(sizePowerUp);
         shouldSpawn = false; 

     }
    } ,
    showHighScore:function(){

        if(this._pointsEarned ==this.highScore -3){
            console.log("Show Highscore");
            var highScoreLabel = cc.LabelTTF.create(this.highScore, "Arial", 10);
            var flower = cc.Sprite.create(s_flower);
      
            this.addChild(highScoreLabel,31);
            
            flower.setPosition(winSize.width  -20, winSize.height + 50); 
            this.addChild(flower,32);
             var actionMove = cc.MoveTo.create(4, cc.p(winSize.width - 20, -50));
             var actionMoveDone = cc.CallFunc.create(function(node) { // 4
           // cc.ArrayRemoveObject(this._blocks, node); // 5
            node.removeFromParent();
           
        }, this);
          
              flower.runAction(cc.Sequence.create(actionMove,actionMoveDone)); 
            
               
        }
        

    }
    ,

    gameLogic:function(dt) {
        
        var shouldSpawn = parseInt((Math.random() * 3));
            if(shouldSpawn == 1){
                shouldSpawn = true; 
                this.spawnPowerUps();
            }

         this.addblock();
         this.showHighScore();

    },

    backgroundLogic:function (dt) {
        this.scrollBackground(); 
    },

 
    firstBackgroundLogic:function(dt){
        this.scrollBackground2();
    },
    onMouseDown:function (event) {
          var location = event.getLocation();
       if( this.pinLocation == true){
        if(location.y <80)
        player.setPosition(location.x, 125);

        if(location.y <80)
        tcircle.setPosition(location.x,location.y);
        }
      
    },

    onTouchesMoved:function (touches, event) {
        console.log("touchesEnded");
        if (touches.length <= 0)
            return;
        var touch = touches[0];
        var location = touch.getLocation();
        if( this.pinLocation == true){
        if(location.y <80)
        player.setPosition(location.x, 125);

        if(location.y <80)
        tcircle.setPosition(location.x,location.y);

        audioEngine.playEffect(s_shootEffect);
        }
    },

    onTouchesMoved:function (touches, event) {
        console.log("Touch moved");
          var location = event.getLocation();
       
    },


    //  onMouseMoved:function(event){
    //     console.log("Mouse moved");
    //     var location = event.getLocation();
    //    if( this.pinLocation == true){
    //     if(location.y <80)
    //     player.setPosition(location.x, 125);

    //     if(location.y <80)
    //     tcircle.setPosition(location.x,location.y);
    //     }
        
    // },
     checkCollision:function(){
        // if (this._powerUps.ICanFly > 0) {
        //     console.log("I can fly!");
        //     return;
        // }
		
		
        var playerRect = player.getBoundingBox(); 
        for(var i = 0; i < this._points.length; i++){
            var point = this._points[i];
            var pointRect = point.getBoundingBox();
            if(cc.rectIntersectsRect(playerRect,pointRect)){
                this._pointsEarned++; 
                score.setString((this._pointsEarned).toString());
                cc.ArrayRemoveObject(this._points, point);
                    gotPoint = true ; 
                    //point.removeFromParent();
                    
            } 
        }

           for (var j = 0; j < this._blocks.length; j++) {
                var block = this._blocks[j];
                var blockRect = block.getBoundingBox();
                
              
                if (cc.rectIntersectsRect(blockRect, playerRect)) {
                    cc.log("collision!");
                  ///Have a logic that does this one time. 
                    this.unschedule(this.gameLogic);
                    this.doEndCountDown ++; 
                    this.showEndCountDown ++ ; 
                    this.showCountDownAfter();
                    this.pinLocation = false; 
                    var keepPlayingButton = cc.Sprite.create(s_keepPlaying);
                    keepPlayingButton.setPosition(winSize.width/2, winSize.height/2);


                    var keepPlayingButton = cc.MenuItemSprite.create(
                    cc.Sprite.create(s_keepPlaying),
                    cc.Sprite.create(s_keepPlaying), 
                    this.transitionScene, this);

                    var menu = cc.Menu.create(keepPlayingButton); 
                   menu.setPosition(winSize.width/2,(winSize.height/2) -100);
                   this.addChild(menu, 45);
                    this.startSecondCountDown = true; 
                     //cc.pauseAllTargets();
                    //cc.ArrayRemoveObject(this._blocks, block);
                   // block.removeFromParent();

                    this._blocksDestroyed++;
                    if (this._blocksDestroyed >= 1) {
                         this.doCountDown = true;
                            this.countDown = 10;
                       
						
						this._blocksDestroyed = 0;
						
						var playerMove =  cc.MoveTo.create(3, cc.p(winSize.width/2, -50));
                        var playerDone = cc.CallFunc.create(function(node) { // 4
                            
                        node.removeFromParent();
                           
                        }, this);
                        player.runAction(cc.Sequence.create(playerMove,playerDone));
						
                       

						
						return false;
                    }

                    
                }

               
                   

            }

			// only can get bee sting if not in stung mode
			if (this._scaleTime == 0) {
				for(var k = 0;k < this._bees.length; k++){
					var bee = this._bees[k];
					var beeRect = bee.getBoundingBox(); 
					if(cc.rectIntersectsRect(beeRect, playerRect)){
						console.log("Player Stung")
						player.setScale(1);
						this._scaleTime = 3;
					}
				}
			}
        
     },

    transitionScene:function(){
         var scene = GameOver.scene(false, this._pointsEarned);
                    player.removeFromParent();
                     cc.Director.getInstance().replaceScene(scene);
                      if(this._pointsEarned > this.highScore){
                           
                            store.set('highScore', this._pointsEarned);
                        }
                     

     },
    clearGame:function(){
        
    },

    showCountDown:function(){
        countDownSprite = cc.Sprite.create(s_countDown);
        countDownSprite.setPosition(winSize.width/2 , winSize.height/2);
        this.addChild(countDownSprite,60);
        countDownLabel = cc.LabelTTF.create("3", "Helvetica", 80);
        countDownLabel.setColor(cc.c3(255,255,255));//black color
        countDownLabel.setPosition(cc.p(winSize.width/2, winSize.height/2));
        this.addChild(countDownLabel,61);
    },

    showCountDownAfter:function(){
        if(this.showEndCountDown == 1){
       var lifeSprite = cc.Sprite.create(s_player0);
       lifeSprite.setPosition(winSize.width/2 + 70, winSize.height/2);
       this.addChild(lifeSprite, 61);
       var  countDownSprite2 = cc.Sprite.create(s_countDown);
        countDownSprite2.setPosition(winSize.width/2 , winSize.height/2);
        this.addChild(countDownSprite2,60);
        countDownLabel2 = cc.LabelTTF.create("10", "Helvetica", 80);
        countDownLabel2.setColor(cc.c3(255,255,255));//black color
        countDownLabel2.setPosition(cc.p(winSize.width/2, winSize.height/2));
        this.addChild(countDownLabel2,61);
        this.showEndCountDown++;
    }
    },

    countDownLogic:function(){
            if(this.doCountDown == true){
            // reduce scaleTime
            if (this._scaleTime > 0) {
             console.log("Scale Time reduce: " + this._scaleTime);
             this._scaleTime--;
            }

            if(this.countDown >= 3){
                //Lable 3 
                console.log("Label:3");
                countDownLabel.setString("3");//Set this as toString of this.CountDown 
                this.countDown--;
            }else if(this.countDown >= 2){
                //lable 2
                console.log("Label:2");
                countDownLabel.setString("2");
                this.countDown--;
            }else if(this.countDown >=1){
                //lable 1 
                console.log("Label:1");
                countDownLabel.setString("1");
                 this.countDown--;
            }else{
                //remove node. 
                console.log("Remove Label");
                 this.countDown--;
                 console.log(this.countDown);
                 countDownSprite.removeFromParent();
                 countDownLabel.removeFromParent();
                 this.doCountDown = false ; 
            }
        }
    },

    endCountDownLogic:function(){
             
            // reduce scaleTime
            if (this._scaleTime > 0) {
             console.log("Scale Time reduce: " + this._scaleTime);
             this._scaleTime--;
            }

            if(this.countDown >= 3){
                //Lable 3 
                console.log("Label:10");
                countDownLabel.setString("10");
                this.countDown--;
            }
            else if(this.countDown >= 2){
                //lable 2
                console.log("Label:2");
                countDownLabel.setString("2");
                this.countDown--;
            }else if(this.countDown >=1){
                //lable 1 
                console.log("Label:1");
                countDownLabel.setString("1");
                 this.countDown--;
            }else{
                //remove node. 
                console.log("Remove Label");
                 this.countDown--;
                 console.log(this.countDown);
                 countDownSprite.removeFromParent();
                 countDownLabel.removeFromParent();
                
            }
        
    } , 

    update:function (dt) {

        // if (this._powerUps.ICanFly > 0) {
        //     console.log("PowerUps Activated");
        //     // Do something
        // }

        this.checkCollision();
       //cc.SPRITE_DEBUG_DRAW =  1;


        var now = (new Date()).getTime();

        if ((now - this._oldTime) > 1000) {
            if (this._powerUps.ICanFly > 0) {
                // Decrese the power ups seconds
                this._powerUps.ICanFly--;
            }

            this.countDownLogic();
            this.playerDied();
			//if this.countDownLogic2 is true , do something 
            this._oldTime = now;
         }
    
		if (this._scaleTime == 0) {
			player.setScale(0.5);
		}

  

          if(this.countDown < 0){
            if(this.backgroundCount < 1){
            this.firstBackgroundLogic();
            this.backgroundCount ++;
        }
            this.schedule(this.gameLogic, 1); // Research more on schedule udpate 
         }

         
    }

});

MainLayer.create = function () {
    var sg = new MainLayer();
    if (sg && sg.init(cc.c4b(200, 255, 100, 230))) {
        return sg;
    }
    return null;
};

MainLayer.scene = function () {
    var scene = cc.Scene.create();
    var layer = MainLayer.create();
    scene.addChild(layer);
    return scene;
};