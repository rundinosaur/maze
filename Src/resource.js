var dirArt = "Art/";
var dirSounds = "Sounds/";

var s_player0 = dirArt + "player0.png";
var s_block = dirArt + "block.png";
var s_touchPad = dirArt + "touchPad.png";
var s_circle = dirArt + "cirlce.png";
var s_tileLayer = dirArt + "tileLayer.tmx";
var s_hud = dirArt + "hud.png";
var s_pointCount = dirArt + "pointBox.png";
var s_playAgain_s = dirArt + "playAgain_s.png";
var s_playAgain_n = dirArt + "playAgain_n.png";
var s_smallPowerUp = dirArt + "smallPowerUp.png";
var s_bigPowerUp = dirArt + "bigPowerUp.png";  
var s_tile7 = dirArt + "tile7.png";
var s_middleTile = dirArt + "middle.png";
var s_leftTile = dirArt + "left.png"; 
var s_rightTile = dirArt + "right.png"; 
var s_oneTile = dirArt + "tile7.png"; 
var s_play_n = dirArt + "menuPlayButton.png";
var s_play_s = dirArt + "menuPlayButton.png";
var s_dare_n = dirArt + "Dare_n.png"; 
var s_dare_s = dirArt + "Dare_s.png"; 
var s_flower = dirArt + "flower.png";
var s_background1 = dirArt + "grassbackground.jpg";
var s_fence = dirArt + "fence.png";
var s_countDown = dirArt + "countDown.png";
var s_player1 = dirArt + "player1.png";
var s_menuPlay_n = dirArt + "menuPlayButton.png";
var s_scoreButton = dirArt + "score.png";
var s_playPlist = dirArt + "player.plist";
var s_player = dirArt + "player.png";
var s_bee0 = dirArt + "bee0.png"; 
var s_beePlist = dirArt + "beePlist.plist";
var s_keepPlaying = dirArt + "keepPlaying.png";
var s_greyFlower = dirArt + "greyFlower.png";

// Sound effect
var s_bgMusic = dirSounds + "background-music.mp3";
var s_bgMusicOgg = dirSounds + "background-music.ogg";
var s_bgMusicCaf = dirSounds + "background-music.caf";

var s_shootEffect = dirSounds + "pew-pew-lei.mp3";
var s_shootEffectOgg = dirSounds + "pew-pew-lei.ogg";
var s_shootEffectWav = dirSounds + "pew-pew-lei.wav";

var g_ressources = [

    {type:"image", src:s_player0},
    {type:"image", src:s_block},
    {type:"image", src:s_touchPad},
    {type:"image", src:s_circle},
    {type:"image", src:s_hud},
    {type:"image", src:s_pointCount},
    {type:"image", src:s_playAgain_s},
    {type:"image", src:s_playAgain_n},
    {type:"image", src:s_smallPowerUp},
    {type:"image", src:s_bigPowerUp},
    {type:"image", src:s_middleTile},
    {type:"image", src:s_leftTile},
    {type:"image", src:s_rightTile},
    {type:"image", src:s_oneTile},
    {type:"image", src:s_playAgain_n},
    {type:"image", src:s_playAgain_s},
    {type:"image", src:s_dare_n},
    {type:"image", src:s_dare_s},
    {type:"image", src:s_flower},
    {type:"image", src:s_background1},
    {type:"image", src:s_fence},
    {type:"image", src:s_countDown},
    {type:"image", src:s_player1},
    {type:"image", src:s_menuPlay_n},
    {type:"image", src:s_scoreButton},
    {type:"plist", src:s_playPlist},
     {type:"image", src:s_player},
     {type:"image", src:s_bee0},
     {type:"plist", src:s_beePlist},
     {type:"image", src:s_keepPlaying},
     {type:"image", src:s_greyFlower},

    // sound effect
    {type:"sound", src:s_bgMusic},
    {type:"sound", src:s_bgMusicOgg},
    {type:"sound", src:s_bgMusicCaf},

    {type:"sound", src:s_shootEffect},
    {type:"sound", src:s_shootEffectOgg},
    {type:"sound", src:s_shootEffectWav}

];