var GameOver = cc.LayerColor.extend({

    _won:false,
    _pointsEarned:0,

    ctor:function() {
        this._super();
        cc.associateWithNative( this, cc.LayerColor );
    },

    onEnter:function () {

        this._super();
         var director = cc.Director.getInstance();
         var winSize = director.getWinSize();
         var background = cc.Sprite.create(s_background1);
        background.setPosition(winSize.width/2 , -550);
        this.addChild(background,0);
        var highScore = store.get("highScore");
        var director = cc.Director.getInstance();
        var winSize = director.getWinSize();
        var centerPos = cc.p( winSize.width/2, winSize.height/2 );
        var selectPlay = cc.Sprite.create(s_play_s);
        var noSelectPlay = cc.Sprite.create(s_play_n);
        var menuItemPlay= cc.MenuItemSprite.create(
            cc.Sprite.create(s_menuPlay_n), 
             cc.Sprite.create(s_menuPlay_n),
            
            this.playAgain, this);


        var dare = cc.MenuItemSprite.create(
             cc.Sprite.create(s_dare_n),
            cc.Sprite.create(s_dare_s), 
            this.dare, this);

         dare.setPosition(0,-75);
       // var menu = cc.Menu.create(menuItemPlay); 
       var tryAgainButton = cc.MenuItemFont.create("Play Again",this.playAgain,this);
            
       //cc.MenuItemFont.create("Play Again",this.playAgain,this);
       
       //tryAgainButton.setColor(cc.c3b(0, 155, 0));
       var menu = cc.Menu.create(tryAgainButton, menuItemPlay, dare); 
       menu.setPosition(winSize.width/2,(winSize.height/2) -100);
       this.addChild(menu, 45);

        var message;
        if (this._won) {
            message = "CHALLENGE";
        } else {
           // message = "HIGHSCORE: \n"+ "20 \n\n"+ "SCORE: \n" + this._pointsEarned +"\n\n";
           message =  "GameOver"; 
        }

        var label = cc.LabelTTF.create(message, "Arial", 32);
        label.setColor(cc.c3b(255, 255, 255));//Black text 
        label.setPosition(winSize.width/2, winSize.height/2);
        this.addChild(label);
        var score = cc.LabelTTF.create("BEST:\n"+ highScore +"\n"+ "SCORE: \n" + this._pointsEarned , "Arial", 40); 
        score.setColor(cc.c3b(255,255,255));
        score.setPosition(winSize.width/2, (winSize.height/2) + 130)
        this.addChild(score);
        // this.runAction(cc.Sequence.create(
        //     cc.DelayTime.create(1),
        //     cc.CallFunc.create(function(node) {
        //         var scene = MainLayer.scene();
        //         cc.Director.getInstance().replaceScene(scene);
        //     }, this)
        // ));

    },

    playAgain:function(){

            console.log("Do something");
		
             var scene = cc.Scene.create();
             scene.addChild(MainLayer.create());
            cc.Director.getInstance().replaceScene(cc.TransitionFade.create(.1,scene));//This needs to create a new scene not create a new instance. 
    },

    dare:function(){
        console.log("InitiateDare");

        window.location.href='http://twitter.com/share?url=http://carpooldigital.com&text=I just scored ' +this._pointsEarned+ ' points in MAZE !&count=horiztonal&via=spoofapps&related=solitarydesigns';
    
    }
});

GameOver.create = function (won , score) {
    var sg = new GameOver();
    sg._won = won;
    sg._pointsEarned = score; 
    if (sg && sg.init(cc.c4b(200, 255, 100, 230))) {
        return sg;
    }
    return null;
};

GameOver.scene = function (won, score) {
    var scene = cc.Scene.create();
    var layer = GameOver.create(won , score);
    scene.addChild(layer);
    return scene;
};