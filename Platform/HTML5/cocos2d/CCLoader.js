/****************************************************************************
 Copyright (c) 2010-2012 cocos2d-x.org
 Copyright (c) 2008-2010 Ricardo Quesada
 Copyright (c) 2011      Zynga Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

/**
 * A class to pre-load resources before engine start game main loop.
 * @class
 * @extends cc.Class
 */
cc.Loader = cc.Class.extend(/**  @lends cc.Loader# */{
    resourceCount:0,
    loadedResourceCount:0,
    timer:0,

    /**
     *  Check the loading status
     */
    isLoadedComplete:function () {
        var loaderCache = cc.Loader.getInstance();
        if (loaderCache.loadedResourceCount == loaderCache.resourceCount) {
            if (loaderCache.onload) {
                loaderCache.timer = setTimeout(loaderCache.onload, 16);
            } else {
                cc.Assert(0, "cocos2d:no load callback defined");
            }
        } else {
            if (loaderCache.onloading) {
                loaderCache.timer = setTimeout(loaderCache.onloading, 16);
            }
            else {
                cc.LoaderScene.getInstance().draw();
            }
            loaderCache.timer = setTimeout(loaderCache.isLoadedComplete, 16);
        }
    },

    /**
     * Callback when loading resource error
     * @param {String} name
     * @example
     * //example
     * cc.Loader.getInstance().onResLoadingErr(name);
     */
    onResLoadingErr:function (name) {
        cc.log("cocos2d:Failed loading resource: " + name);
    },

    /**
     *Callback when a resource file loaded.
     * @example
     * //example
     * cc.Loader.getInstance().onResLoaded();
     */
    onResLoaded:function () {
        this.loadedResourceCount++;
    },

    /**
     *  For loading percentage
     *  You can use this method to create a custom loading screen.
     * @return {Number}
     * @example
     * //example
     * cc.log(cc.Loader.getInstance().getProgressBar() + "%");
     */
    getProgressBar:function () {
        var per = this.loadedResourceCount / this.resourceCount;
        per = 0 | (per * 100);
        return per;
    },

    /**
     * status when resources loading success
     * @example
     *  //example
     * cc.Loader.getInstance().onload = function () {
     *      cc.AppController.shareAppController().didFinishLaunchingWithOptions();
     * };
     */
    onload:undefined,

    /**
     *  status when res loading error
     * @example
     * //example
     * cc.Loader.getInstance().onerror = function () {
     *      //do something
     * };
     */
    onerror:undefined,

    /**
     *  status when res loading
     * @example
     * //example
     * cc.Loader.getInstance().onloading = function () {
     *       cc.LoaderScene.getInstance().draw();
     * };
     */
    onloading:undefined,

    _registerFaceFont:function (fontRes) {
        var srcArr = fontRes.srcArr;
        if (fontRes.srcArr && srcArr.length > 0) {
            var fontStyle = document.createElement("style");
            fontStyle.type = "text/css";
            document.body.appendChild(fontStyle);

            var fontStr = "@font-face { font-family:" + fontRes.fontName + "; src:";
            for (var i = 0; i < srcArr.length; i++) {
                fontStr += "url('" + encodeURI(srcArr[i].src) + "') format('" + srcArr[i].type + "')";
                fontStr += (i == (srcArr.length - 1)) ? ";" : ",";
            }
            fontStyle.textContent += fontStr + "};";
        }
        cc.Loader.getInstance().onResLoaded();
    },

    /**
     * Pre-load the resources before engine start game main loop.
     * There will be some error without pre-loading resources.
     * @param {object} res
     * @example
     * //example
     * var res = [
     *               {type:"image", src:"hello.png"},
     *               {type:"tmx", src:"hello.tmx"}
     *     ]
     * cc.Loader.getInstance().preload(res);
     */
    preload:function (res) {
        var sharedTextureCache = cc.TextureCache.getInstance();
        var sharedEngine = cc.AudioEngine.getInstance();
        var sharedParser = cc.SAXParser.getInstance();
        var sharedFileUtils = cc.FileUtils.getInstance();

        this.loadedResourceCount = 0;
        this.resourceCount = res.length;
        for (var i = 0; i < res.length; i++) {
            switch (res[i].type) {
                case "image":
                    sharedTextureCache.addImage(res[i].src);
                    break;
                case "sound":
                    sharedEngine.preloadSound(res[i].src);
                    break;
                case "plist":
                case "tmx":
                case "fnt":
                    sharedParser.preloadPlist(res[i].src);
                    break;
                case "tga":
                    //cc.log("cocos2d:not implemented yet")
                    break;
                case "ccbi":
                case "binary":
                    sharedFileUtils.preloadBinaryFileData(res[i].src);
                    break;
                case "face-font":
                    this._registerFaceFont(res[i]);
                    break;
                default:
                    throw "cocos2d:unknow type : " + res[i].type;
                    break;
            }
        }
        this.isLoadedComplete();
    }
});

/**
 * Share Loader
 * @return {cc.Loader}
 */
cc.Loader.getInstance = function () {
    if (!this._instance) {
        this._instance = new cc.Loader();
    }
    return this._instance;
};
cc.Loader._instance = null;

/**
 * Default loading screen, you can customize the loading screen.
 * @class
 * @extends cc.Class
 */
cc.LoaderScene = cc.Class.extend(/**  @lends cc.LoaderScene# */{
    _logo:new Image(),

    /**
     * Constructor
     */
    ctor:function () {
        this._logo.src = "logo.png";

        this._logo.width = 160;
        this._logo.height = 200;
    },

    /**
     * Draw loading screen
     */
    draw:function () {
        var logoWidth = (cc.canvas.width - this._logo.width) / 2;
        var logoHeight = (cc.canvas.height - this._logo.height) / 2;
        cc.renderContext.clearRect(0, -cc.canvas.height, cc.canvas.width, cc.canvas.height);
        cc.renderContext.fillStyle = "#202020";
        cc.renderContext.fillRect(0, -cc.canvas.height, cc.canvas.width, cc.canvas.height);
        cc.drawingUtil.drawImage(this._logo, cc.p(logoWidth, logoHeight));
        cc.renderContext.fillStyle = "#b2b4b3";
        cc.renderContext.font = 'Bold 12px Verdana';
        cc.renderContext.textAlign = 'left';
        cc.drawingUtil.fillText("Loading " + cc.Loader.getInstance().getProgressBar() + "%", logoWidth + 30, logoHeight - 15);
    }
});

/**
 * Shared loader scene
 * @return {cc.LoaderScene}
 */
cc.LoaderScene.getInstance = function () {
    if (!this._instance) {
        this._instance = new cc.LoaderScene();
    }
    return this._instance;
};

cc.LoaderScene._instance = null;